package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)

const (
	host = "localhost"
	port = 5432
	user = "marcin"
	dbname = "postgresGolang"
)

type Server struct {
	db Database
	cache Cache
}

type Grade struct {
	Subject string `json:"subject"`
	SingleGrade int `json:"grade"`
}

type Grades struct {
	AllGrades []Grade `json:"gradesList"`
}

func (g *Grades) isEmpty() bool{

	if len(g.AllGrades) > 0 {
		return false
	}

	return true
}

func (s *Server) GradesHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		result, _ := s.cache.GetGrades()

		if result.isEmpty(){
			//read from database and update cache
			currentGrades, _ := s.db.GetGrades()
			_ = s.cache.SaveGrades(currentGrades)
			output, _ := json.Marshal(currentGrades)
			_, _ = fmt.Fprintf(w, string(output))
		} else {
		output, _ := json.Marshal(result)
		_, _ = fmt.Fprintf(w, string(output))
		}
	}
}


func (s *Server) RootHandler() http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {
		_, _ = fmt.Fprintf(w, "This is a sample webpage")
	}
}

func (s *Server) routes(){
	http.HandleFunc("/", s.RootHandler())
	http.HandleFunc("/grades", s.GradesHandler())
}

func main() {
	redisCache := RedisCacher{
		redisClient: redis.NewClient(&redis.Options{
			Addr: "localhost:6379",
			Password: "",
			DB: 0,
		}),
	}

	dbinfo := fmt.Sprintf("dbname=%s sslmode=disable", dbname)
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	postgresDatabase := PostgresConnector{postgresDb:db}

	s := Server{
		db: &postgresDatabase,
		cache: &redisCache,
	}

	s.routes()
	log.Fatal(http.ListenAndServe(":8888", nil))
	}