package main

import (
	"github.com/go-redis/redis"
	"strconv"
)

type Cache interface {
	GetGrades() (*Grades, error)
	SaveGrades(grades *Grades) error
}

type RedisCacher struct {
	redisClient *redis.Client
}

func (c *RedisCacher) GetGrades() (*Grades, error) {
	keys, _ := c.redisClient.Keys("*").Result()
	var listOfGrades []Grade

	for _, element := range keys{
		gradesListLength, _ := c.redisClient.LLen(element).Result()
		subjectGrades, _ := c.redisClient.LRange(element, 0, gradesListLength-1).Result()

		for i := int64(0); i<gradesListLength; i++{
			gradeValue, _ := strconv.Atoi(subjectGrades[i])
			grade := Grade{element, gradeValue}
			listOfGrades = append(listOfGrades, grade)
			}
	}

	output := &Grades{listOfGrades}
	return output, nil
}

func (c *RedisCacher) SaveGrades(grades *Grades) error {
	list := grades.AllGrades

	for index := range list{
		c.redisClient.RPush(list[index].Subject, list[index].SingleGrade)
	}

	return nil
}

