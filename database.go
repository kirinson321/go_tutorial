package main

import "database/sql"

type Database interface {
	//save to database
	//...TODO
	//read from database
	GetGrades() (*Grades, error)
	SaveGrades(grades *Grades) error
}

type PostgresConnector struct {
	postgresDb *sql.DB
}


func (db *PostgresConnector) GetGrades() (*Grades, error) {
	//need to check up the args parameter, for it specifies the number of records getting returned
	rows, err := db.postgresDb.Query("SELECT * FROM grades")
	if err != nil{
		panic(err)
	}
	defer rows.Close()

	var listOfGrades []Grade

	for rows.Next(){
		var subject string
		var singlegrade int

		err = rows.Scan(&subject, &singlegrade)
		if err != nil {
			panic(err)
		}

		grade := Grade{subject, singlegrade}
		listOfGrades = append(listOfGrades, grade)
	}

	output := &Grades{listOfGrades}
	return output, nil
}

func (db *PostgresConnector) SaveGrades(grades *Grades) error {
	listOfGrades := grades.AllGrades
	sqlStatement := `INSERT INTO grades (subject, grade)
	VALUES ($1, $2)`
	for _, element := range listOfGrades{
		gradeSingleGrade := element.SingleGrade
		gradeSubject := string(element.Subject)
		err := db.postgresDb.QueryRow(sqlStatement, 2, gradeSubject, gradeSingleGrade)
		if err != nil{
			panic(err)
		}
		}
	return nil
}